import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MomoLinkageComponent } from './momo-linkage.component';

describe('MomoLinkageComponent', () => {
  let component: MomoLinkageComponent;
  let fixture: ComponentFixture<MomoLinkageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MomoLinkageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MomoLinkageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
