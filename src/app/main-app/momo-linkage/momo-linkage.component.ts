import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiServiceService} from '../../services/api-service.service';
import {ToastServiceService} from '../../services/toast-service.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {StorageServiceService} from '../../services/storage-service.service';
import {CustomValidationServiceService} from '../../services/custom-validation-service.service';

@Component({
  selector: 'app-momo-linkage',
  templateUrl: './momo-linkage.component.html',
  styleUrls: ['./momo-linkage.component.scss']
})
export class MomoLinkageComponent implements OnInit {

  @Input() title: string;
  @Input() accountNumber: string;
  accountName: string;
  @Output() onCancel = new EventEmitter();
  @Output() onSubmit = new EventEmitter();
  reviewed = false;
  agreementFormGroup: FormGroup;
  myFormGroup: FormGroup;
  @Input() type: string;
  accountResp: any;
  currency: string;
  accountVerified = false;
  obj: any;


  constructor(private formBuilder: FormBuilder,
              public apiService: ApiServiceService,
              private toastService: ToastServiceService,
              private storageService: StorageServiceService,
              private customValidationService: CustomValidationServiceService,
              private spinner: NgxSpinnerService) {

    if (!this.obj){
      this.obj = {
        stage: 'services-submit',
        serviceData: ''
      };
    }


  }

  ngOnInit(): void {
    this.agreementFormGroup = this.formBuilder.group({
      terms: ['', Validators.required]
    });



    this.myFormGroup = this.formBuilder.group({
      accountNumber: [],
      mobileNo: ['',  [Validators.required, this.customValidationService.checkLimit(100000000, 9999999999)]],
      operator: [],
      name: [],
      type: [this.type]



    });
  }


  review(): void{
   this.getAccountData();
  }

  cancel(): void{
    this.onCancel.emit('service-select');
  }

  submit(): void{
    this.obj.serviceData = this.myFormGroup.value;
    this.obj.serviceData.branch = this.storageService.getBranch();
    this.onSubmit.emit(this.obj);

  }

  back(): void{
    this.reviewed = false;
    this.myFormGroup.enable();

  }

  getAccountData(): void{
    const accountNumberLength = this.myFormGroup.get('accountNumber').value.toString().length;
    if(accountNumberLength === 13){
      this.spinner.show('accountSpinner');
      this.apiService.getAccountData(this.myFormGroup.get('accountNumber').value).subscribe(res =>{
        this.accountResp = res;
        this.spinner.hide('accountSpinner');
        if(this.accountResp.hostHeaderInfo.responseCode === '000'){
          const verify = this.verifyAccountName(this.accountResp.accountName);
          if (verify){
            this.reviewed = true;
            this.myFormGroup.disable();
            this.title = 'Summary';
          }
          else{
            this.toastService.showToast('Sorry. Invalid account credentials', 'error-toast');
          }
        }
        else{
          this.toastService.showToast('Invalid account credentials try again', 'error-toast');
        }
      },error => {
        this.spinner.hide('accountSpinner');
        this.toastService.showToast('Sorry couldn\'t account data', 'error-toast');
      });
    }
  }


  verifyAccountName(accName: any): boolean{

    let state = false;
    const userName = this.myFormGroup.get('name').value.toString().toUpperCase();
    const info = userName.split(' ');
    const count = info.length;

    for (let i = 0; i < count; i++){
      state = accName.includes(info[i]);

      if (state){
        break;
      }
      else if ((!state) && (info[i].length !== count)){
        continue;
      }

    }

    return state;

  }

}
