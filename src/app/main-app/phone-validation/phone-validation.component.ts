import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-phone-validation',
  templateUrl: './phone-validation.component.html',
  styleUrls: ['./phone-validation.component.scss']
})
export class PhoneValidationComponent implements OnInit {

  @Output() onValidate = new EventEmitter();
  emailPhone = new FormControl('', Validators.required);

  constructor() { }

  ngOnInit(): void {
  }

  validate(): void {
    this.onValidate.emit(this.emailPhone.value);
  }
}
