import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-customer-type',
  templateUrl: './customer-type.component.html',
  styleUrls: ['./customer-type.component.scss']
})
export class CustomerTypeComponent implements OnInit {

  @Output() onCustomerSelect = new EventEmitter();
  user = new FormControl('', Validators.required);
  obj: any;


  constructor() {


    if (!this.obj){
      this.obj = {
        stage: 'phone-data',
        userType: ''
      };
    }
  }

  ngOnInit(): void {
  }

  start(): void{
    this.obj.userType = this.user.value;
    this.onCustomerSelect.emit(JSON.stringify(this.obj));
  }

}
