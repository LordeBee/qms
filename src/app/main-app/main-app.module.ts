import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexModule} from '@angular/flex-layout';
import {MainAppComponent} from './main-app/main-app.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {IConfig, NgxMaskModule} from 'ngx-mask';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {HttpClientModule} from '@angular/common/http';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatButtonModule} from '@angular/material/button';
import { WelcomeComponent } from './welcome/welcome.component';
import { CustomerTypeComponent } from './customer-type/customer-type.component';
import { AccountValidationComponent } from './account-validation/account-validation.component';
import { PhoneValidationComponent } from './phone-validation/phone-validation.component';
import { OtpValidationComponent } from './otp-validation/otp-validation.component';
import { ServicesGridComponent } from './services-grid/services-grid.component';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { AccountNumberOnlyServicesComponent } from './account-number-only-services/account-number-only-services.component';
import { CashDepositComponent } from './cash-deposit/cash-deposit.component';
import { CashRedrawalComponent } from './cash-redrawal/cash-redrawal.component';
import { ChequeDepositComponent } from './cheque-deposit/cheque-deposit.component';
import { RemitannceComponent } from './remitannce/remitannce.component';
import { MobileMoneyComponent } from './mobile-money/mobile-money.component';
import { ExternalTransfersComponent } from './external-transfers/external-transfers.component';
import { OnlineBankingServicesComponent } from './online-banking-services/online-banking-services.component';
import { MomoLinkageComponent } from './momo-linkage/momo-linkage.component';
import { MoneyWalletComponent } from './money-wallet/money-wallet.component';
import { PhoneNumberServicesComponent } from './phone-number-services/phone-number-services.component';
import { SuccessPageComponent } from './success-page/success-page.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AlternativeModalComponent } from './alternative-modal/alternative-modal.component';
import { InternalTransfersComponent } from './internal-transfers/internal-transfers.component';
import { BranchesDialogComponent } from './branches-dialog/branches-dialog.component';
import { MobileDialogComponent } from './mobile-dialog/mobile-dialog.component';
import { SchoolFeesComponent } from './school-fees/school-fees.component';
import { GeneralInquiryComponent } from './general-inquiry/general-inquiry.component';



@NgModule({
  declarations: [MainAppComponent, WelcomeComponent, CustomerTypeComponent, AccountValidationComponent, PhoneValidationComponent, OtpValidationComponent, ServicesGridComponent, AccountNumberOnlyServicesComponent, CashDepositComponent, CashRedrawalComponent, ChequeDepositComponent, RemitannceComponent, MobileMoneyComponent, ExternalTransfersComponent, OnlineBankingServicesComponent, MomoLinkageComponent, MoneyWalletComponent, PhoneNumberServicesComponent, SuccessPageComponent, AlternativeModalComponent, InternalTransfersComponent, BranchesDialogComponent, MobileDialogComponent, SchoolFeesComponent, GeneralInquiryComponent],
  entryComponents: [AlternativeModalComponent, BranchesDialogComponent, MobileDialogComponent],
  imports: [
    CommonModule,
    FlexModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxMaskModule.forRoot(),
    MatCheckboxModule,
    MatRadioModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    HttpClientModule,
    MatSnackBarModule,
    MatDialogModule,
    MatButtonModule,
    MatToolbarModule,
    TypeaheadModule.forRoot(),
    FormsModule,
    NgxSpinnerModule
  ]
})
export class MainAppModule {
}
