import { Component, OnInit } from '@angular/core';
import {ApiServiceService} from '../../services/api-service.service';
import {ToastServiceService} from '../../services/toast-service.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {MatDialog} from '@angular/material/dialog';
import {AlternativeModalComponent} from '../alternative-modal/alternative-modal.component';
import {StorageServiceService} from '../../services/storage-service.service';
import {ActivatedRoute} from '@angular/router';
import {BranchesDialogComponent} from '../branches-dialog/branches-dialog.component';
interface QMSAppsInterface {
  id: any;
  type: any;
  alternative: any;
  url: any;
  description: any;
  chargeType: any;
  charge: any;
  customerOnly: any;
}

@Component({
  selector: 'app-main-app',
  templateUrl: './main-app.component.html',
  styleUrls: ['./main-app.component.scss']
})
export class MainAppComponent implements OnInit {


  appTitle = 'Stanbic Bank';
  stage = 'start';
  userType = '';
  accountNumberValidation: any;
  phoneEmailValidation: any;
  otpObj: any;
  qmsApps: Array<any>;
  appsResp: any;
  selectedService: QMSAppsInterface;
  hostHeaderInfo: any;
  saveResp: any;
  ticketNumber: any;
  userApps: Array<QMSAppsInterface>;

  gridBoolean = false;
  alt: any;
  branch: any;

  constructor(public apiService: ApiServiceService,
              private toastService: ToastServiceService,
              private storageService: StorageServiceService,
              private spinner: NgxSpinnerService,
              private route: ActivatedRoute,
              public dialog: MatDialog) {
    if(!this.hostHeaderInfo){
      this.hostHeaderInfo = {
        ipAddress: '127.0.0.1',
        requestId: '1',
        sourceChannelId: 'QMS'
      };
    }

    this.ticketNumber = '';
  }

  ngOnInit(): void {
    // this.storageService.saveBranch('AIRPORT CITY');
    this.route.queryParams.subscribe(params => {
      this.branch = params.type;
      if(this.branch === 'branch'){
        this.dialog.open( BranchesDialogComponent, {
          disableClose: true,
          width: '500px',
          height: '250px'
        });
      }

    });
    this.getApps();

  }

  welcomeActions(stage: any): void{
    this.stage = stage;
  }

  customerType(stage: any): void{
    const data = JSON.parse(stage);
    this.stage = data.stage;
    this.userType = data.userType;
    if(this.userType === 'Y'){
      this.userApps = this.qmsApps;
    }
    else {
      this.userApps = this.qmsApps.filter(type => type['customerOnly'] === 'N');
    }
  }

  getApps(): void{
    this.apiService.getQMSApps().subscribe(res => {
      this.appsResp = res;
      console.log(this.appsResp);
      if(this.appsResp.hostHeaderInfo.responseCode === '000'){
          this.qmsApps  = this.appsResp.serviceTypeInfo;
      }
      else{
        this.toastService.showToast('Sorry couldn\'t load services', 'error-toast');
      }
    },error => {
      console.log(error)
      this.toastService.showToast('Sorry couldn\'t load services', 'error-toast');
    });
  }

  getSelectedService(service: any): void{

      this.stage = service.stage;
      this.selectedService = service.serviceData;
      this.alt = this.selectedService.alternative;
  }

  serviceCancel(stage: any): void{
    this.stage = stage;
    this.selectedService = null;
  }


  serviceSubmit(data: any): void{
    const obj = {hostHeaderInfo: '', qms: ''};
    obj.hostHeaderInfo = this.hostHeaderInfo;
    obj.qms = data.serviceData;
    console.log(obj)
    this.spinner.show('submissionSpinner');

    this.apiService.saveQMSInfo(obj).subscribe(res => {
      this.saveResp = res;
      this.spinner.hide('submissionSpinner');
      if(this.saveResp.hostHeaderInfo.responseCode === '000'){
        this.stage = 'success';
        this.ticketNumber = this.saveResp.ticketNumber;
      }
      else{
        this.toastService.showToast('Sorry couldn\'t queue your request', 'error-toast');
      }


    },error => {
      console.log(error)
      this.spinner.hide('submissionSpinner');
      this.toastService.showToast('Sorry  couldn\'t queue your request', 'error-toast');
    });

  }

  exit(): void{
    this.selectedService = null;
    this.stage = 'start';
    this.gridBoolean = false;
  }

}
