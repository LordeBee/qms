import {Component, OnInit, EventEmitter, Output, Input} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {TypeaheadMatch} from 'ngx-bootstrap/typeahead';

interface QMSAppsInterface {
  id: any;
  type: any;
  alternative: any;
  url: any;
  description: any;
  chargeType: any;
  charge: any;
  customerOnly: any;
}


@Component({
  selector: 'app-services-grid',
  templateUrl: './services-grid.component.html',
  styleUrls: ['./services-grid.component.scss']
})
export class ServicesGridComponent implements OnInit {



  @Output() onSelectService = new EventEmitter();
  @Input() customerType: string;
  @Input() qmsApps: Array<QMSAppsInterface>;
  selected: string;
  obj: any;
  @Input()serviceSelect = false;
  serviceApps = ['Account Services', 'Cash Services', 'Card services', 'Cheque Services', 'Forex Services', 'Payment Services', 'Remittance Services', 'Inquiries'];
  selectedPackage = 0;
  filteredService: Array<QMSAppsInterface>;



  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {




    iconRegistry.addSvgIcon(
      'remittance',
      sanitizer.bypassSecurityTrustResourceUrl('/workFlow//assets/icons/remittance.svg'));

    iconRegistry.addSvgIcon(
      'general',
      sanitizer.bypassSecurityTrustResourceUrl('/workFlow//assets/icons/general.svg'));




    iconRegistry.addSvgIcon(
      'accounts',
      sanitizer.bypassSecurityTrustResourceUrl('/workFlow/assets/icons/acc-services-icon.svg'));

    iconRegistry.addSvgIcon(
      'cash',
      sanitizer.bypassSecurityTrustResourceUrl('/workFlow/assets/icons/cash-services-icon.svg'));

    iconRegistry.addSvgIcon(
      'loan',
      sanitizer.bypassSecurityTrustResourceUrl('/workFlow/assets/icons/card-services-icon.svg'));

    iconRegistry.addSvgIcon(
      'cheque',
      sanitizer.bypassSecurityTrustResourceUrl('/workFlow/assets/icons/cheque-services-icon.svg'));

    iconRegistry.addSvgIcon(
      'forex',
      sanitizer.bypassSecurityTrustResourceUrl('/workFlow/assets/icons/forex-services-icon.svg'));

    iconRegistry.addSvgIcon(
      'payment',
      sanitizer.bypassSecurityTrustResourceUrl('/workFlow/assets/icons/payment-services-icon.svg'));



    iconRegistry.addSvgIcon(
      'thumb-one',
      sanitizer.bypassSecurityTrustResourceUrl('/workFlow/assets/icons/service-thumb-one.svg'));

    iconRegistry.addSvgIcon(
      'thumb-two',
      sanitizer.bypassSecurityTrustResourceUrl('/workFlow/assets/icons/service-thumb-two.svg'));

    iconRegistry.addSvgIcon(
      'thumb-three',
      sanitizer.bypassSecurityTrustResourceUrl('/workFlow/assets/icons/service-thumb-three.svg'));

    iconRegistry.addSvgIcon(
      'thumb-four',
      sanitizer.bypassSecurityTrustResourceUrl('/workFlow/assets/icons/service-thumb-four.svg'));

    if (!this.obj){
      this.obj = {
        stage: 'services',
        serviceData: ''
      };
    }
  }

  ngOnInit(): void {
  }

  select(packageName: any): void{
    this.serviceSelect = true;
    this.selectedPackage = packageName;
    if(packageName === 0){
      this.filteredService = this.qmsApps.filter(
        type => (type['id'] === '19' || type['id'] === '17' || type['id'] === '12' || type['id'] === '11' || type['id'] === '10' || type['id'] === '9' || type['id'] === '8' )
      );
    }
    else if(packageName === 1){
      this.filteredService = this.qmsApps.filter(
        type => (type['id'] === '1' || type['id'] === '2' || type['id'] === '3' || type['id'] === '4' || type['id'] === '5' )
      );
    }
    else if(packageName === 2){
      this.filteredService = this.qmsApps.filter(
        type => (type['id'] === '13' || type['id'] === '14' || type['id'] === '20' || type['id'] === '22')
      );
    }
    else if(packageName === 3){
      this.filteredService = this.qmsApps.filter(
        type => (type['id'] === '16' || type['id'] === '23' || type['id'] === '18'  || type['id'] === '25'  || type['id'] === '26')
      );
    }

    else if(packageName === 4){
      this.filteredService = this.qmsApps.filter(
        type => (type['id'] === '7')
      );
    }

    else if(packageName === 5){
      this.filteredService = this.qmsApps.filter(
        type => (type['id'] === '6' || type['id'] === '24' || type['id'] === '31')
      );
    }

    else if(packageName === 6){
      this.filteredService = this.qmsApps.filter(
        type => (type['id'] === '27' || type['id'] === '28' || type['id'] === '29')
      );
    }

    else if(packageName === 7){
      this.filteredService = this.qmsApps.filter(
        type => (type['id'] === '30')
      );
    }
    // this.onSelect.emit(packageName);
  }

  onSelect(data: TypeaheadMatch): void{
    this.obj.serviceData  = data.item;
    this.onSelectService.emit(this.obj);
  }

  selectedPackaged(data: any): void{
    this.obj.serviceData  = data;
    this.onSelectService.emit(this.obj);
  }

  getThumb(idx): string{
    if(idx === 0 || idx === 6){
      return 'thumb-one';
    }
    else if(idx === 1 || idx === 5){
      return 'thumb-two';
    }
    else if(idx === 2 || idx === 4){
      return 'thumb-three';
    }
    else if(idx === 3  || idx === 7){
      return 'thumb-four';
    }
  }

  back(): void{
    this.serviceSelect = false;
  }

}
