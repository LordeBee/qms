import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemitannceComponent } from './remitannce.component';

describe('RemitannceComponent', () => {
  let component: RemitannceComponent;
  let fixture: ComponentFixture<RemitannceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemitannceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemitannceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
