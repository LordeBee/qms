import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiServiceService} from '../../services/api-service.service';
import {ToastServiceService} from '../../services/toast-service.service';
import {StorageServiceService} from '../../services/storage-service.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {APP_DATE_FORMATS , AppDateAdapter} from '../../services/format-datepicker';
import {DateAdapter , MAT_DATE_FORMATS} from '@angular/material/core';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-remitannce',
  providers: [DatePipe, {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}],
  templateUrl: './remitannce.component.html',
  styleUrls: ['./remitannce.component.scss']
})
export class RemitannceComponent implements OnInit {
  title: string;
  @Input() accountNumber: string;
  accountName: string;
  @Output() onCancel = new EventEmitter();
  @Output() onSubmit = new EventEmitter();
  reviewed = false;
  agreementFormGroup: FormGroup;
  myFormGroup: FormGroup;
  @Input() type: string;
  accountResp: any;
  currency: string;
  accountVerified = false;
  obj: any;
  inWords: any;
  @Input() id: string;
  todayDate:Date = new Date();

  constructor(private formBuilder: FormBuilder,
              public apiService: ApiServiceService,
              private toastService: ToastServiceService,
              private datePipe: DatePipe,
              private storageService: StorageServiceService,
              private spinner: NgxSpinnerService) {

    if (!this.obj){
      this.obj = {
        stage: 'services-submit',
        serviceData: ''
      };
    }


  }

  ngOnInit(): void {
    this.agreementFormGroup = this.formBuilder.group({
      terms: ['', Validators.required]
    });

    this.myFormGroup = this.formBuilder.group({
      name: [],
      dob: [],
      address: [],
      mobileNo: [],
      country: [],
      occupation: [],
      totalAmount: [],
      type: [this.type],
      idNumber: [],
      idType: [],
      relationshipSender: [],
      transferReason: [],
      testAnswer: [],
      accountNumber: [],
      currency: []



    });
  }

  review(): void{
    this.reviewed = true;
    this.myFormGroup.disable();
    this.title = 'Summary';
    this.convertValues(this.myFormGroup.get('totalAmount').value);
  }

  cancel(): void{
    this.onCancel.emit('service-select');
  }

  submit(): void{

    this.obj.serviceData = this.myFormGroup.value;
    this.obj.serviceData.dob = this.datePipe.transform(this.myFormGroup.get('dob').value, 'y-MMM-d');
    this.obj.serviceData.branch = this.storageService.getBranch();
    this.onSubmit.emit(this.obj);

  }

  back(): void{
    this.reviewed = false;
    this.myFormGroup.enable();

  }

  convertValues(value: any): void{
    let resp;
    if(value) {
      this.apiService.convertNumber(value).subscribe(res =>{
        console.log(res)
        resp = res;
        this.inWords = resp.value;
      })
    }
  }

}
