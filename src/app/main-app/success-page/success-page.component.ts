import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MobileDialogComponent} from '../mobile-dialog/mobile-dialog.component';
import {ToastServiceService} from '../../services/toast-service.service';
import {TitleCasePipe} from '@angular/common';
import {StorageServiceService} from '../../services/storage-service.service';
import {ApiServiceService} from '../../services/api-service.service';


@Component({
  selector: 'app-success-page',
  providers: [TitleCasePipe],
  templateUrl: './success-page.component.html',
  styleUrls: ['./success-page.component.scss']
})
export class SuccessPageComponent implements OnInit {


  @Input() ticketNumber: any;
  @Output() onDone = new EventEmitter();
  @Input() alt: any;
  sent = false;

  constructor(public dialog: MatDialog , private toastService: ToastServiceService , private titleCae: TitleCasePipe, private storageService: StorageServiceService, private apiService: ApiServiceService) { }

  ngOnInit(): void {

    console.log(this.alt)
    this.send();
  }

  done(): void{
    this.onDone.emit('start');
  }

  openDialog(): void{
    const dialogRef = this.dialog.open( MobileDialogComponent, {
      disableClose: true,
      width: '400px',
      height: '220px',
      data: {ticketNumber: this.lpad(this.ticketNumber, 3)}
    });

    dialogRef.componentInstance.onApprove.subscribe(res => {
      if(res){
        this.done();
      }
    })
  }


  lpad(value, padding) {
    const zeroes = new Array(padding + 1).join('0');
    return (zeroes + value).slice(-padding);
  }
  
  
  send(): void{
    const body = {
      hostHeaderInfo: {
        ipAddress: 'string',
        requestId: 'string',
        sourceChannelId: 'string'
      },
      phone: sessionStorage.getItem('phone'),
      branch: this.titleCae.transform(this.storageService.getBranch().replace('BRANCH' ,'')),
      ticketNumber: this.lpad(this.ticketNumber, 3)
    };


    this.apiService.sendSMS(body).subscribe((res: any) => {
      if (res.hostHeaderInfo.responseCode === '000'){
        this.toastService.showToast('Your ticket number has been sent successfully', 'success-toast');
      }
      else{
        this.toastService.showToast('Sorry couldn\'t send ticket', 'error-toast');
      }

    }, error => {
      console.log(error)

      this.toastService.showToast('Sorry couldn\'t send ticket number', 'error-toast');
    });

  }

}
