import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalTransfersComponent } from './external-transfers.component';

describe('ExternalTransfersComponent', () => {
  let component: ExternalTransfersComponent;
  let fixture: ComponentFixture<ExternalTransfersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalTransfersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalTransfersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
