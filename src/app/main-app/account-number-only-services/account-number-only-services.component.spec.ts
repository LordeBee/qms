import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountNumberOnlyServicesComponent } from './account-number-only-services.component';

describe('AccountNumberOnlyServicesComponent', () => {
  let component: AccountNumberOnlyServicesComponent;
  let fixture: ComponentFixture<AccountNumberOnlyServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountNumberOnlyServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountNumberOnlyServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
