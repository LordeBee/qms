import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-money-wallet',
  templateUrl: './money-wallet.component.html',
  styleUrls: ['./money-wallet.component.scss']
})
export class MoneyWalletComponent implements OnInit {
  @Input() title: string;
  @Input() accountNumber: string;
  accountName: string;
  @Output() onCancel = new EventEmitter();
  @Output() onSubmit = new EventEmitter();
  reviewed = false;
  agreementFormGroup: FormGroup;
  myFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder) {

    this.title = 'Money Wallet Upload';
    this.accountNumber = '9040007184616';
    this.accountName = 'BELINDA ADUSEI NUAMAH';
  }

  ngOnInit(): void {
    this.agreementFormGroup = this.formBuilder.group({
      terms: ['', Validators.required]
    });

    this.myFormGroup = this.formBuilder.group({
      twoHrdNotes: [],
      oneHrdNotes: [],
      fiftyNotes: [],
      twentyNotes: [],
      tenNotes: [],
      fiveNotes: [],
      twoNotes: [],
      oneNotes: [],
      oneCoin: [],
      fiftyCoin: [],
      twentyCoin: [],
      tenCoin: [],
      fiveCoin: [],
      onePCoin: [],
      type: [],
      idNumber: [],
      idType: [],
      amount: [],
      depositorPhone: [],
      accountNumber: [],
      accountName: [],



    });
  }


  review(): void{
    this.reviewed = true;
    this.myFormGroup.disable();
    this.title = 'Summary';
  }

  cancel(): void{
    this.onCancel.emit('services');
  }

  submit(): void{

    this.onSubmit.emit('services-submit');

  }

  back(): void{
    this.reviewed = false;
    this.myFormGroup.enable();
    this.title = 'Money Wallet Upload';
  }

}
