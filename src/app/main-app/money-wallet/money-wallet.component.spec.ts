import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoneyWalletComponent } from './money-wallet.component';

describe('MoneyWalletComponent', () => {
  let component: MoneyWalletComponent;
  let fixture: ComponentFixture<MoneyWalletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoneyWalletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoneyWalletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
