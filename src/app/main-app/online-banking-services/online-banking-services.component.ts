import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiServiceService} from '../../services/api-service.service';
import {ToastServiceService} from '../../services/toast-service.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {StorageServiceService} from '../../services/storage-service.service';
import {AlternativeModalComponent} from '../alternative-modal/alternative-modal.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-online-banking-services',
  templateUrl: './online-banking-services.component.html',
  styleUrls: ['./online-banking-services.component.scss']
})
export class OnlineBankingServicesComponent implements OnInit {

  @Input() title: string;
  @Input() accountNumber: string;
  accountName: string;
  @Output() onCancel = new EventEmitter();
  @Output() onSubmit = new EventEmitter();
  reviewed = false;
  agreementFormGroup: FormGroup;
  myFormGroup: FormGroup;
  accountVerified = false;
  obj: any;
  @Input() type: string;
  @Input() id: any;
  accountResp: any;
  currency: string;
  usernameResp: any;



  constructor(private formBuilder: FormBuilder,
              public apiService: ApiServiceService,
              private toastService: ToastServiceService,
              private storageService: StorageServiceService,
              public dialog: MatDialog,
              private spinner: NgxSpinnerService) {

    if (!this.obj) {
      this.obj = {
        stage: 'services-submit',
        serviceData: ''
      };
    }

  }

  ngOnInit(): void {
    this.agreementFormGroup = this.formBuilder.group({
      terms: ['', Validators.required]
    });


    if (this.id === '8') {
      this.myFormGroup = this.formBuilder.group({
        accountNumber: [],
        type: [this.type],
        name: [],
        username: ['', Validators.required]
      });
    } else {
      this.myFormGroup = this.formBuilder.group({
        accountNumber: [],
        name: [],
        type: [this.type],
      });
    }

  }


  review(): void {
    this.getAccountData();
  }

  cancel(): void {
    this.onCancel.emit('service-select');
  }

  submit(): void {

    this.obj.serviceData = this.myFormGroup.value;
    this.obj.serviceData.branch = this.storageService.getBranch();
    this.onSubmit.emit(this.obj);

  }

  back(): void {
    this.reviewed = false;
    this.myFormGroup.enable();
  }

  getAccountData(): void {
    const accountNumberLength = this.myFormGroup.get('accountNumber').value.toString().length;
    if (accountNumberLength === 13) {
      this.spinner.show('accountSpinner');
      this.apiService.getAccountData(this.myFormGroup.get('accountNumber').value).subscribe(res => {
        this.accountResp = res;
        this.spinner.hide('accountSpinner');

        if (this.accountResp.hostHeaderInfo.responseCode === '000') {
          const verify = this.verifyAccountName(this.accountResp.accountName);
          if (verify){
            this.apiService.getFebAccountData(this.myFormGroup.get('accountNumber').value).subscribe(resp => {
              this.spinner.hide('accountSpinner');
              if (resp.hostHeaderInfo.responseCode === 'A10' && this.id === '9' ) {
                const dialogRef =  this.dialog.open(AlternativeModalComponent,
                  {disableClose: true,
                    data:
                      {message: 'Oops! Looks like you have not been set up yet.', subtext: 'Would you like to sign up instead?'}
                  });

                dialogRef.componentInstance.onDecline.subscribe(res => {
                  if (res){
                    this.cancel();
                  }
                });

                dialogRef.componentInstance.onApprove.subscribe(res => {
                  if (res){
                    this.accept();
                  }
                });
              }
              else if (resp.hostHeaderInfo.responseCode === '000' && this.id === '8' ) {
                const dialogRef =   this.dialog.open(AlternativeModalComponent,
                  {disableClose: true,
                    data:
                      {message: 'Oops! Looks like you have  been set up already.', subtext: 'Would you like to reset your account instead?'}
                  });

                dialogRef.componentInstance.onDecline.subscribe(res => {
                  if (res){
                    this.cancel();
                  }
                });

                dialogRef.componentInstance.onApprove.subscribe(res => {
                  if (res){
                    this.accept();
                  }
                });

              }

              else{
                this.getUserName();
              }


            });
          }
          else{
            this.toastService.showToast('Sorry. Invalid account credentials', 'error-toast');
          }




        } else {
          this.toastService.showToast('Invalid account credentials try again', 'error-toast');
        }
      }, error => {
        this.spinner.hide('accountSpinner');
        this.toastService.showToast('Sorry couldn\'t account data', 'error-toast');
      });
    }
  }

  getUserName(): void {

    if (this.id === '8') {
      this.spinner.show('validation');
      this.apiService.validateUsername(this.myFormGroup.get('username').value).subscribe(res => {
        this.usernameResp = res;
        this.spinner.hide('validation');
        if(this.usernameResp.responseCode === 'A22'){
          this.reviewed = true;
          this.myFormGroup.disable();
          this.title = 'Summary';
        } else {
          this.toastService.showToast('Oops username already taken. Please choose another', 'error-toast');
        }
      }, error => {
        this.spinner.hide('validation');
        this.toastService.showToast('Sorry couldn\'t validate username', 'error-toast');
      });
    } else {
      this.reviewed = true;
      this.myFormGroup.disable();
      this.title = 'Summary';
    }

  }

  accept(): void{
    if (this.id === '8'){
      this.id = '9';
      this.type = 'Reset Online & Mobile Baking';
      this.formsInit();
    }
    else  if (this.id === '9'){
      this.id = '8';
      this.type = 'Setup Online & Mobile Banking ';
      this.formsInit();
    }
  }

  formsInit(): void{

    if (this.id === '8') {
      this.myFormGroup = this.formBuilder.group({
        accountNumber: [this.accountResp.accountNumber],
        type: [this.type],
        name: [this.accountResp.accountName],
        username: ['', Validators.required]
      });
    } else {
      this.myFormGroup = this.formBuilder.group({
        accountNumber: [this.accountResp.accountNumber],
        name: [this.accountResp.accountName],
        type: [this.type],
      });
    }
  }


  verifyAccountName(accName: any): boolean{

    let state = false;
    const userName = this.myFormGroup.get('name').value.toString().toUpperCase();
    const info = userName.split(' ');
    const count = info.length;

    for (let i = 0; i < count; i++){
      state = accName.includes(info[i]);

      if (state){
        break;
      }
      else if ((!state) && (info[i].length !== count)){
        continue;
      }

    }

    return state;

  }


}
