import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineBankingServicesComponent } from './online-banking-services.component';

describe('OnlineBankingServicesComponent', () => {
  let component: OnlineBankingServicesComponent;
  let fixture: ComponentFixture<OnlineBankingServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlineBankingServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineBankingServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
