import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {ApiServiceService} from '../../services/api-service.service';
import {StorageServiceService} from '../../services/storage-service.service';

@Component({
  selector: 'app-branches-dialog',
  templateUrl: './branches-dialog.component.html',
  styleUrls: ['./branches-dialog.component.scss']
})
export class BranchesDialogComponent implements OnInit {

  selectedBranch: any;
  branches: Array<any>;
  resp: any;
  constructor(public dialogRef: MatDialogRef<BranchesDialogComponent>, private apiService: ApiServiceService, public storageService: StorageServiceService) {
    this.selectedBranch = '';
    this.branches = [];
  }

  ngOnInit(): void {
    this.selectedBranch = this.storageService.getBranch();
    this.apiService.getBranches().subscribe(res => {
      this.resp = res;
      if (this.resp.hostHeaderInfo.responseCode === '000'){
        this.branches = this.resp.fiBranchInfo;
      }
    });
  }

  close(): void{
    this.storageService.saveBranch(this.selectedBranch);
    this.dialogRef.close();
  }

}
