import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {CustomValidationServiceService} from '../../services/custom-validation-service.service';

@Component({
  selector: 'app-account-validation',
  templateUrl: './account-validation.component.html',
  styleUrls: ['./account-validation.component.scss']
})
export class AccountValidationComponent implements OnInit {

  @Output() onValidate = new EventEmitter();
  accountNumber = new FormControl('',  [Validators.required, this.customValidationService.checkLimit(100000000, 9999999999)]);
  obj: any;

  constructor(private customValidationService: CustomValidationServiceService) {

    if (!this.obj){
      this.obj = {
        stage: 'service-select',

      };
    }
  }

  ngOnInit(): void {

  }

  validate(): void {
   sessionStorage.setItem('phone', this.accountNumber.value);
   this.accountNumber.setValue('');
   this.onValidate.emit(this.obj.stage);
  }

}
