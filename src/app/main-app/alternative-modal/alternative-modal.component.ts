import {Component, Inject, OnInit, Output, EventEmitter} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

interface Data{
  message: string;
  subtext: any;
}

@Component({
  selector: 'app-alternative-modal',
  templateUrl: './alternative-modal.component.html',
  styleUrls: ['./alternative-modal.component.scss']
})
export class AlternativeModalComponent implements OnInit {

  @Output() onApprove = new EventEmitter();
  @Output() onDecline = new EventEmitter();

  constructor(public dialogRef: MatDialogRef<AlternativeModalComponent>, @Inject(MAT_DIALOG_DATA) public data: Data) { }

  ngOnInit(): void {
  }


  onNoClick(): void {
    this.onDecline.emit('000');
    this.dialogRef.close();
  }

  accept(): void{
    this.onApprove.emit('000');
    this.dialogRef.close();
  }

}
