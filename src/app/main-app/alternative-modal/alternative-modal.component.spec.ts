import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlternativeModalComponent } from './alternative-modal.component';

describe('AlternativeModalComponent', () => {
  let component: AlternativeModalComponent;
  let fixture: ComponentFixture<AlternativeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlternativeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlternativeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
