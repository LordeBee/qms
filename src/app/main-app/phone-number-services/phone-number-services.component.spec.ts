import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhoneNumberServicesComponent } from './phone-number-services.component';

describe('PhoneNumberServicesComponent', () => {
  let component: PhoneNumberServicesComponent;
  let fixture: ComponentFixture<PhoneNumberServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhoneNumberServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhoneNumberServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
