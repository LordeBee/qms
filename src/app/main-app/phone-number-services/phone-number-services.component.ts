import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StorageServiceService} from '../../services/storage-service.service';

@Component({
  selector: 'app-phone-number-services',
  templateUrl: './phone-number-services.component.html',
  styleUrls: ['./phone-number-services.component.scss']
})
export class PhoneNumberServicesComponent implements OnInit {
  @Input() title: string;
  @Input() accountNumber: string;
  accountName: string;
  @Output() onCancel = new EventEmitter();
  @Output() onSubmit = new EventEmitter();
  reviewed = false;
  agreementFormGroup: FormGroup;
  myFormGroup: FormGroup;
  @Input() type: string;
  obj: any;
  @Input() fees: any;

  constructor(private storageService: StorageServiceService, private formBuilder: FormBuilder) {

    if (!this.obj){
      this.obj = {
        stage: 'services-submit',
        serviceData: ''
      };
    }


  }

  ngOnInit(): void {
    this.agreementFormGroup = this.formBuilder.group({
      terms: ['', Validators.required]
    });

    this.myFormGroup = this.formBuilder.group({
      mobileNo: [],
      type: [this.type]



    });
  }


  review(): void{
    this.reviewed = true;
    this.myFormGroup.disable();
    this.title = 'Summary';
  }

  cancel(): void{
    this.onCancel.emit('service-select');
  }

  submit(): void{

    this.obj.serviceData = this.myFormGroup.value;
    this.obj.serviceData.branch = this.storageService.getBranch();
    this.onSubmit.emit(this.obj);

  }

  back(): void{
    this.reviewed = false;
    this.myFormGroup.enable();

  }

}

