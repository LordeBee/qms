import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-otp-validation',
  templateUrl: './otp-validation.component.html',
  styleUrls: ['./otp-validation.component.scss']
})
export class OtpValidationComponent implements OnInit {

  otpFormGroup: FormGroup;
  @Output() onValidate = new EventEmitter();
  @Output() onResend = new EventEmitter();
  @Input() endedTimer: boolean;

  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit(): void {

    $(".inputs").on("keyup", function (e) {
      if (this.value.length == this.maxLength) {
        $(this).next(".inputs").focus();
      }

      if (e.which == 8 || e.which == 46) {
        $(this).prev(".inputs").focus();
      }
    });


    this.otpFormGroup = this._formBuilder.group({
      c1: ['', Validators.required],
      c2: ['', Validators.required],
      c3: ['', Validators.required],
      c4: ['', Validators.required],
      c5: ['', Validators.required]
    });
  }

  validate(): void{
    this.onValidate.emit(this.otpFormGroup.value);
  }

  resendOTP(): void{
    this.onResend.emit(true);
  }

}
