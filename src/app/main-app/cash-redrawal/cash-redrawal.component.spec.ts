import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashRedrawalComponent } from './cash-redrawal.component';

describe('CashRedrawalComponent', () => {
  let component: CashRedrawalComponent;
  let fixture: ComponentFixture<CashRedrawalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashRedrawalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashRedrawalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
