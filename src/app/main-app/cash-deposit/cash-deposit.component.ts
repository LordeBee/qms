import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiServiceService} from '../../services/api-service.service';
import {ToastServiceService} from '../../services/toast-service.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {CustomValidationServiceService} from '../../services/custom-validation-service.service';
import {StorageServiceService} from '../../services/storage-service.service';

interface ServiceData {
  accountNumber: any;
  currency: any;
  idNumber: any;
  idType: any;
  name: any;
  username: any;
  totalAmount: any;
  type: any;
  mobileNo: any;
  denomination: any;
}

@Component({
  selector: 'app-cash-deposit',
  templateUrl: './cash-deposit.component.html',
  styleUrls: ['./cash-deposit.component.scss']
})
export class CashDepositComponent implements OnInit {

  @Input() title: string;
  @Input() accountNumber: string;
  accountName: string;
  @Output() onCancel = new EventEmitter();
  @Output() onSubmit = new EventEmitter();
  reviewed = false;
  agreementFormGroup: FormGroup;
  myFormGroup: FormGroup;
  @Input() id: string;
  @Input() type: string;

  notesFormGroup: any;
  isReadyNotes = false;

  coinsFormGroup: any;
  isReadyCoins = false;
  accountResp: any;
  accountVerified = false;
  currency: any;
  totalVal = 0;
  inWords = '';


  notesArray: Array<any>;
  coinsArray: Array<any>;
  obj: any;

  notesDenomination = [
    {type: 'notes', value: 200},
    {type: 'notes', value: 100},
    {type: 'notes', value: 50},
    {type: 'notes', value: 20},
    {type: 'notes', value: 10},
    {type: 'notes', value: 5},
    {type: 'notes', value: 2},
    {type: 'notes', value: 1}
  ];

  coinsDenomination = [
    {type: 'coins', value: 1, cur: 'GHS 1'},
    {type: 'coins', value: 0.50, cur: '50p'},
    {type: 'coins', value: 0.20, cur: '20p'},
    {type: 'coins', value: 0.10, cur: '10p'},
    {type: 'coins', value: 0.05, cur: '5p'},
    {type: 'coins', value: 0.01, cur: '1p'}
  ];

  constructor(private formBuilder: FormBuilder, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,
              public apiService: ApiServiceService,
              private toastService: ToastServiceService,
              private spinner: NgxSpinnerService,
              private storageService: StorageServiceService,
              private customValidationService: CustomValidationServiceService) {

    iconRegistry.addSvgIcon(
      'notes',
      sanitizer.bypassSecurityTrustResourceUrl('/workFlow/assets/icons/deposit-cash.svg'));

    iconRegistry.addSvgIcon(
      'coins',
      sanitizer.bypassSecurityTrustResourceUrl('/workFlow/assets/icons/deposit-coins.svg'));
    this.currency = '';
    this.coinsArray = [];
    this.notesArray = [];

    if (!this.obj) {
      this.obj = {
        stage: 'services-submit',
        serviceData: ''
      };
    }


  }

  ngOnInit(): void {
    this.agreementFormGroup = this.formBuilder.group({
      terms: ['', Validators.required]
    });


    if (this.id === '3') {
      this.myFormGroup = this.formBuilder.group({
        accountNumber: [],
        currency: [],
        idNumber: [],
        idType: [],
        name: [],
        username: [],
        totalAmount: [],
        type: [this.type],
        mobileNo: ['', [Validators.required, this.customValidationService.checkLimit(100000000, 9999999999)]],
        address: [],


      });
    } else if (this.id === '2') {
      this.myFormGroup = this.formBuilder.group({
        accountNumber: [],
        currency: [],
        name: [],
        username: [],
        totalAmount: [],
        type: [this.type],
        mobileNo: ['', [Validators.required, this.customValidationService.checkLimit(100000000, 9999999999)]]


      });
    }


    this.createFormCoins();
    this.createFormNotes();
  }

  createFormNotes(): void {
    this.notesFormGroup = this.formBuilder.group({
      notes: this.initItemsNotes()
    });
    this.isReadyNotes = true;
    console.log('form created', this.notesFormGroup);
  }

  createFormCoins(): void {
    this.coinsFormGroup = this.formBuilder.group({
      coins: this.initItemsCoins()
    });
    this.isReadyCoins = true;
    console.log('form created', this.coinsFormGroup);
  }

  initItemsNotes(): FormArray {
    const formArray = this.formBuilder.array([]);
    const count = this.notesDenomination.length;
    for (let i = 0; i < count; i++) {
      formArray.push(this.formBuilder.group({
        amount: [''],
        count: [''],
        currency: [''],
        value: [this.notesDenomination[i].value],
        id: [0],
        type: [this.notesDenomination[i].type]
      }));
    }
    return formArray;
  }


  initItemsCoins(): FormArray {
    const formArray = this.formBuilder.array([]);
    const count = this.coinsDenomination.length;
    for (let i = 0; i < count; i++) {
      formArray.push(this.formBuilder.group({
        amount: [''],
        count: [''],
        currency: [''],
        value: [this.coinsDenomination[i].value],
        id: [0],
        type: [this.coinsDenomination[i].type]
      }));
    }
    return formArray;
  }


  review(): void {

    this.getAccountData();
  }


  calculations(): void{

    const notesValue = this.notesFormGroup.get('notes').value;
    const coinsValue = this.coinsFormGroup.get('coins').value;
    const notesTotal = notesValue.reduce((accum, item) => accum + Number(item.count * item.value), 0);
    const coinsTotal = coinsValue.reduce((accum, item) => accum + Number(item.count * item.value), 0);

    this.notesArray = this.notesFormGroup.get('notes').value;
    const notesCount = this.notesArray.length;
    for (let i = 0; i < notesCount; i++){
      this.notesArray[i].amount = this.getCount(this.notesArray[i].count, this.notesDenomination[i].value);
    }

    this.coinsArray = this.coinsFormGroup.get('coins').value;
    const coinsCount = this.coinsArray.length;
    for (let i = 0; i < coinsCount; i++){
      this.coinsArray[i].amount = this.getCount(this.coinsArray[i].count, this.coinsArray[i].value);
    }

    const total = notesTotal + coinsTotal;
    this.myFormGroup.get('totalAmount').setValue(total);
    this.totalVal = total;
    this.convertValues(total);
  }

  cancel(): void {

    this.onCancel.emit('service-select');
  }

  submit(): void {

    const denomination = this.notesArray.concat(this.coinsArray);

    this.obj.serviceData = this.myFormGroup.value;
    this.obj.serviceData.denomination = denomination;
    this.obj.serviceData.branch = this.storageService.getBranch();
    this.onSubmit.emit(this.obj);

  }

  back(): void {
    this.reviewed = false;
    this.myFormGroup.enable();

  }


  getAccountData(): void{
    const accountNumberLength = this.myFormGroup.get('accountNumber').value.toString().length;
    if (accountNumberLength === 13) {
      this.spinner.show('accountSpinner');
      this.apiService.getAccountData(this.myFormGroup.get('accountNumber').value).subscribe(res => {
        this.accountResp = res;
        this.spinner.hide('accountSpinner');
        if (this.accountResp.hostHeaderInfo.responseCode === '000') {
          const verify = this.verifyAccountName(this.accountResp.accountName);
          if (verify){
            this.reviewed = true;
            this.myFormGroup.disable();
            this.title = 'Summary';
            this.myFormGroup.get('currency').setValue(this.accountResp.currencyCode);
            this.currency = this.accountResp.currencyCode;
            this.calculations();
          }
          else{
            this.toastService.showToast('Sorry. Invalid account credentials', 'error-toast');
          }


          if (this.id === '2') {
            this.myFormGroup.get('username').setValue(this.accountResp.accountName);
          }
        } else {
          this.toastService.showToast('Invalid account credentials try again', 'error-toast');
        }
      }, error => {
        this.spinner.hide('accountSpinner');
        this.toastService.showToast('Sorry couldn\'t account data', 'error-toast');
      });
    }
  }

  getCount(amount: any, value: number): any {
    if (amount) {
      const amnt = Number(amount);
      return (amnt * value);
    }
    else {
      return  0;
    }

  }


  convertValues(value: any): void {
    let resp;
    if (value) {
      this.apiService.convertNumber(value).subscribe(res => {
        console.log(res)
        resp = res;
        this.inWords = resp.value;
      })
    }
  }



  verifyAccountName(accName: any): boolean{

    let state = false;
    const userName = this.myFormGroup.get('name').value.toString().toUpperCase();
    const info = userName.split(' ');
    const count = info.length;

    for (let i = 0; i < count; i++){
      state = accName.includes(info[i]);

      if (state){
        break;
      }
      else if ((!state) && (info[i].length !== count)){
        continue;
      }

    }

    return state;

  }


}
