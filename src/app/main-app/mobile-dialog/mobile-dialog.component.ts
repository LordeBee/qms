import {Component , OnInit , Output , EventEmitter , Inject} from '@angular/core';
import {MAT_DIALOG_DATA , MatDialogRef} from '@angular/material/dialog';
import {TitleCasePipe} from '@angular/common';
import {StorageServiceService} from '../../services/storage-service.service';
import {ApiServiceService} from '../../services/api-service.service';
import {ToastServiceService} from '../../services/toast-service.service';


export interface ApproveDialogData {
  ticketNumber: string;



}

@Component({
  providers: [TitleCasePipe],
  selector: 'app-mobile-dialog',
  templateUrl: './mobile-dialog.component.html',
  styleUrls: ['./mobile-dialog.component.scss']
})
export class MobileDialogComponent implements OnInit {

  mobileNo = '';

  loading = false;

  @Output() onApprove = new EventEmitter();
  constructor( private toastService: ToastServiceService, @Inject(MAT_DIALOG_DATA) public data: ApproveDialogData, public dialogRef: MatDialogRef<MobileDialogComponent>, private titleCae: TitleCasePipe, private storageService: StorageServiceService, private apiService: ApiServiceService) { }

  ngOnInit(): void {
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  action(): void{
    if(this.mobileNo.toString().length > 9){
      const body = {
        hostHeaderInfo: {
          ipAddress: 'string',
          requestId: 'string',
          sourceChannelId: 'string'
        },
        phone: this.mobileNo,
        branch: this.titleCae.transform(this.storageService.getBranch().replace('BRANCH' ,'')),
        ticketNumber: this.data.ticketNumber
      };
      this.loading = true;

      this.apiService.sendSMS(body).subscribe((res: any) => {
        this.loading = false;
        if(res.hostHeaderInfo.responseCode === '000'){
          this.onApprove.emit('000');
          this.toastService.showToast('Your ticket number has been sent successfully', 'success-toast');
          this.onNoClick();

        }
        else{
          this.toastService.showToast('Sorry couldn\'t send ticket', 'error-toast');
        }

      },error => {
        console.log(error)
        this.loading = false;
        this.toastService.showToast('Sorry couldn\'t send ticket number', 'error-toast');
      });
    }

    else{
      this.toastService.showToast('Please enter a valid mobile number', 'error-toast');
    }

  }




}
