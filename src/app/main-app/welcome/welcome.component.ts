import {Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  @Output() onStart = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }



  start(): void{
    this.onStart.emit('customer-type');
  }

}
