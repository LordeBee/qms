import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {CustomValidationServiceService} from '../../services/custom-validation-service.service';
import {ApiServiceService} from '../../services/api-service.service';
import {StorageServiceService} from '../../services/storage-service.service';

@Component({
  selector: 'app-general-inquiry',
  templateUrl: './general-inquiry.component.html',
  styleUrls: ['./general-inquiry.component.scss']
})
export class GeneralInquiryComponent implements OnInit {
  @Input() title: string;
  @Input() accountNumber: string;
  accountName: string;
  @Output() onCancel = new EventEmitter();
  @Output() onSubmit = new EventEmitter();
  reviewed = false;
  agreementFormGroup: FormGroup;
  myFormGroup: FormGroup;
  @Input() type: string;
  obj: any;
  inWords = '';

  constructor(private formBuilder: FormBuilder,  public apiService: ApiServiceService, private storageService: StorageServiceService,
              private customValidationService: CustomValidationServiceService) {


    this.accountNumber = '9040007184616';
    this.accountName = 'BELINDA ADUSEI NUAMAH';

    if (!this.obj){
      this.obj = {
        stage: 'services-submit',
        serviceData: ''
      };
    }
  }

  ngOnInit(): void {
    this.agreementFormGroup = this.formBuilder.group({
      terms: ['', Validators.required]
    });


    this.myFormGroup = this.formBuilder.group({
      inquiryType: [],
      name: [],
      mobileNo: ['',  [Validators.required, this.customValidationService.checkLimit(100000000, 9999999999)]],
      type: [this.type],




    });
  }


  review(): void{
    this.reviewed = true;
    this.myFormGroup.disable();
    this.title = 'Summary';

  }

  cancel(): void{
    this.onCancel.emit('service-select');
  }

  submit(): void{

    this.obj.serviceData = this.myFormGroup.value;
    this.obj.serviceData.branch = this.storageService.getBranch();
    this.onSubmit.emit(this.obj);

  }

  back(): void{
    this.reviewed = false;
    this.myFormGroup.enable();

  }

  convertValues(value: any): void{
    let resp;
    if(value) {
      this.apiService.convertNumber(value).subscribe(res =>{
        console.log(res)
        resp = res;
        this.inWords = resp.value;
      })
    }
  }

}
