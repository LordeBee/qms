import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';


@Injectable({
  providedIn: 'root'
})
export class ToastServiceService {

  constructor(private _snackBar: MatSnackBar) { }

  showToast(message: string,type: string) {

    this._snackBar.open(message, '', {duration:10000,verticalPosition: 'top',panelClass: [type]});

  }
}
