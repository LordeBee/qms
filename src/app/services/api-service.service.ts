import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  // private deployBase = '';
  private deployBase = '';
  private baseURL = `${this.deployBase}/paperlessProxy/`;
  private baseURL2 = `${this.deployBase}/branchProxy/`;
  private saveQMSInfoUrl = `${this.baseURL}QmsService/api/v1/request`;
  private getQMSAppsUrl = `${this.baseURL}QmsService/api/v1/apps`;
  private getAccountsUrl = `${this.baseURL}QmsService/api/v1/account?accountNumber=`;
  private validateUsernameUrl = `${this.baseURL}QmsService/api/v1/verifyUsername?username=`;
  private convertNumberUrl = `${this.baseURL}AccountServices/accounts/amountwords?amount=`;
  private getBranchesUrl = `${this.baseURL2}BankServices/fi/branches?type=branch`;
  private febAccount = `${this.baseURL}QmsService/api/v1/febaccount?accountNumber=`;
  private smsUrl = `${this.baseURL}QmsService/api/v1/sms`;


  private httpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')
    .set('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT')
    .set(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    )
    .set('countryCode', 'GH')
    .set('sourceCode', 'QMS')
    .set('requestToken', '123456')
    .set('Cache-Control', 'no-cache');


  constructor(private http: HttpClient) {

  }

  private requestHeaders = {
    headers: this.httpHeaders
  };


  getQMSApps(): any {
    return this.http.get(this.getQMSAppsUrl, this.requestHeaders);
  }

  getAccountData(accountNumber: any): any {
    return this.http.get(this.getAccountsUrl + accountNumber, this.requestHeaders);
  }

  getFebAccountData(accountNumber): any {
    return this.http.get(`${this.febAccount}${accountNumber}`, this.requestHeaders)
  }

  validateUsername(accountNumber: any): any {
    return this.http.get(this.validateUsernameUrl + accountNumber, this.requestHeaders);
  }


  saveQMSInfo(data: any): any {
    const info = JSON.stringify(data);
    return this.http.post(this.saveQMSInfoUrl, info, this.requestHeaders);
  }

  convertNumber(value): any {
    return this.http.get(this.convertNumberUrl + value, this.requestHeaders);
  }

  getBranches(): any {
    return this.http.get(this.getBranchesUrl, this.requestHeaders);
  }

  sendSMS(data: any): any {
    const info = JSON.stringify(data);
    return this.http.post(this.smsUrl, info, this.requestHeaders);
  }


}
