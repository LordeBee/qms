import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class StorageServiceService {
  userRole:any;

  constructor(private cookieService: CookieService,private router:Router) {
    this.userRole= {
      role:''
    }
  }


  saveBranch(branch: any): void{
    sessionStorage.setItem('branch', branch);
  }

  getBranch(): string{
    if(sessionStorage.getItem('branch')){
      return sessionStorage.getItem('branch');
    }
    else{
      return '';
    }
  }

  saveUserData(data:any){
    const userData =  JSON.stringify(data);
    this.cookieService.set("userObj",userData);
  }

  getData(){
    const userData = this.cookieService.get("userObj");
    return JSON.parse(userData);
  }

  loggedIn():boolean{
    const userData = this.cookieService.get("userObj");
    if(userData){
      return true;
    }else {
      return false;
    }
  }

  getUserRole(){
    if(this.cookieService.get("userObj")){
      const userData = JSON.parse(this.cookieService.get("userObj"));
      if (userData.apps) {
        var count = userData.apps.length;
        for (var i = 0; i < count; i++) {
          if (userData.apps[i].app === 'MOBY CREDIT') {
            this.userRole = {role : userData.apps[i].role};
            return this.userRole;
          }
        }

      }
    }else{
      return this.userRole;
    }

  }

  logout(){
    this.cookieService.delete('userObj');
    this.router.navigate(['/login']);
  }
}
