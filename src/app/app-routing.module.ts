import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainAppComponent} from './main-app/main-app/main-app.component';


const routes: Routes = [
  {path: 'app', component: MainAppComponent},
  {path: '', pathMatch: 'full', redirectTo: 'app'},
  {path: '***', pathMatch: 'full', redirectTo: 'app'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
